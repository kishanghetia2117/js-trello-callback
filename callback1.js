/* 
    Problem 1: Write a function that will return a particular board's information 
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const getSpecificBordInfo = (bordDataSet, BordId, callback) => {
    setTimeout(()=> {
        let specificBordInfo= bordDataSet.find((bord) => bord.id === BordId);

        if (specificBordInfo) {
            callback(specificBordInfo,null);
        } else {
            callback(null,'id not found');
        }
    },2000)
    
}
module.exports = getSpecificBordInfo;