/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const bordDataSet = require('../data/boards.json');
const listDataSet = require('../data/lists.json');
const cardsDataSet = require('../data/cards.json');

const getThanosBoardListCArd = require('../callback5.js');
const getSpecificBordInfo = require('../callback1.js');
const getListOfBordId = require('../callback2.js');
const getCardOfListId = require('../callback3.js');

const data = [bordDataSet,listDataSet,cardsDataSet];

getThanosBoardListCArd(getSpecificBordInfo,getListOfBordId,getCardOfListId,'mcu453ed',bordDataSet,listDataSet,cardsDataSet);

