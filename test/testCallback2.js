/* 
	Problem 2: Write a function that will return all lists that belong to a board 
    based on the boardID that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const listDataSet = require('../data/lists.json');
const getListOfBordId = require('../callback2.js');

const callback = (data,err) => {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}
getListOfBordId(listDataSet,'mcu453ed',callback);

