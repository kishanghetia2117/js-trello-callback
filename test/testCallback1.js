/* 
	Problem 1: Write a function that will return a particular board's information 
    based on the boardID that is passed from the given list of boards in boards.json 
    and then pass control back to the code that called it by using a callback function.
*/

const bordDataSet = require('../data/boards.json');
const getSpecificBordInfo = require('../callback1.js');

const callback = (data,err) => {
    if(err) {
        console.log(err);
    } else {
        console.log(data);
    }
}
getSpecificBordInfo(bordDataSet,'mcu453ed',callback);

