/* 
    Problem 2: Write a function that will return all lists that belong to a board 
    based on the boardID that is passed to it from the given data in lists.json. 
    Then pass control back to the code that called it by using a callback function.
*/

const getListOfBordId = (listDataSet, BordId, callback) => {
    
    setTimeout(() => {
        let ListOfBordId = listDataSet[BordId];
        if (ListOfBordId) {
            callback(ListOfBordId, null);
        } else {
            callback(null, 'list not found');
        }
    }, 2000)
}
module.exports = getListOfBordId;