/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. 
    You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/
const getThanosBoardListCArd = (getSpecificBordInfo, getListOfBordId, getCardOfListId, ThanosId, bordDataSet, listDataSet, cardsDataSet) => {

    setTimeout(() => {
        getSpecificBordInfo(bordDataSet, ThanosId, (data, err) => {
            if (err) {
                console.log(err);
            } else {
                console.log(data);

                getListOfBordId(listDataSet, data.id, (data, err) => {
                    if (err) {
                        console.log(err);
                    } else {
                        console.log(data);
                        data.forEach((stone, index) => {
                            getCardOfListId(cardsDataSet, stone.id, (data, err) => {
                                if (err) {
                                    console.log(err);
                                } else {
                                    console.log(data);
                                }
                            }, (index + 1) * 2)
                        })
                    }
                });
            }
        });
    }, 2000)
}

module.exports = getThanosBoardListCArd;